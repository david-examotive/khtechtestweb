const seasonDict = {
    SPR: "1",
    SUM: "2",
    AUT: "3",
    WIN: "4"
};
const monthsDict = {
    JAN: "01",
    FEB: "02",
    MAR: "03",
    APR: "04",
    MAY: "05",
    JUN: "06",
    JUL: "07",
    AUG: "08",
    SEP: "09",
    OCT: "10",
    NOV: "11",
    DEC: "12"
};

const seasons = ['SPR', 'SUM', 'AUT', 'WIN'];

export var yearSeasonTranslation = function (data) {
    let translatedData = [];
    let dataMap = {};
    for (var datapoint of data) {
        if ('Year' in datapoint && 'region' in datapoint) {
            for (let i in seasons) {
                let season = seasons[i];
                if (season in datapoint) {
                    let seasonString = `${datapoint['Year']}.${seasonDict[season]}`;
                    let existingData = { "season" : seasonString };
                    if (seasonString in dataMap) {
                        existingData = dataMap[seasonString];
                    }
                    dataMap[seasonString] = Object.assign(existingData, { [datapoint["region"]] : datapoint[season] });
                }
            }
        }
    }
    for (var key in dataMap) {
        translatedData.push(dataMap[key]);
    }
    return translatedData;
}

export var monthSplit = function(data) {
    let translatedData = [];
    let datapoint = data[0];
    for (var property in datapoint) {
        if (datapoint.hasOwnProperty(property)) {
            if (property in monthsDict) {
                translatedData.push({ month : monthsDict[property], value : datapoint[property]})
            }
        }
    }
    return translatedData;
}

export var seasonSplit = function(data) {
    let translatedData = [];
    let datapoint = data[0];
    for (var property in datapoint) {
        if (datapoint.hasOwnProperty(property)) {
            if (property in seasonDict) {
                translatedData.push({ season : property, value : datapoint[property]})
            }
        }
    }
    return translatedData;
}