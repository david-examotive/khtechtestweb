import { yearSeasonTranslation } from './data-translations.js'
import { validData } from '../tests/data/api-data.js'
import { multiRegionSeason } from '../tests/data/expected-results.js'

test('valid data for multi region season', () => {
    expect(yearSeasonTranslation(validData)).toEqual(multiRegionSeason);
  });