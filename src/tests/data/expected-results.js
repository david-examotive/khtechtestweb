export const multiRegionSeason = [
    {"season": "1925.1", "UK" : 10.54, "England": 11.57}, 
    {"season": "1925.2", "UK" : 18.88, "England": 20.28},
    {"season": "1925.3", "UK" : 11.15, "England": 12.02}, 
    {"season": "1925.4", "UK" : 7.44, "England": 8.01},
    {"season": "1926.1", "UK" : 11.32, "England": 12.18}, 
    {"season": "1926.2", "UK" : 18.53, "England": 19.75},
    {"season": "1926.3", "UK" : 11.82, "England": 13.01}, 
    {"season": "1926.4", "UK" : 6.4, "England": 6.95}
]
