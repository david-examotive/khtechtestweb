export const validData = [
    {
        "id": "5a88d224e02d8f7697e93eba", "dataset": "tmax", "region": "UK", "Year": 1925,
        "JAN": 7.1, "FEB": 6.7, "MAR": 7.3, "APR": 10.3, "MAY": 14.0, "JUN": 18.5, "JUL": 19.9, "AUG": 18.2, "SEP": 14.1,
        "OCT": 12.9, "NOV": 6.4, "DEC": 4.8, "WIN": 7.44, "SPR": 10.54, "SUM": 18.88, "AUT": 11.15, "ANN": 11.72
    },
    {
        "id": "5a88d224e02d8f7697e93ebc", "dataset": "tmax", "region": "UK", "Year": 1926, "JAN": 6.5, "FEB": 8.1,
        "MAR": 8.7, "APR": 12.2, "MAY": 13.1, "JUN": 16.7, "JUL": 19.7, "AUG": 19.2, "SEP": 17.0, "OCT": 10.5, "NOV": 8.0,
        "DEC": 6.3, "WIN": 6.4, "SPR": 11.32, "SUM": 18.53, "AUT": 11.82, "ANN": 12.18
    },
    {
        "id": "5a88d224e02d8f7697e93f94", "dataset": "tmax", "region": "England", "Year": 1925, "JAN": 7.6, "FEB": 7.7, "MAR": 8.0,
        "APR": 11.2, "MAY": 15.5, "JUN": 19.9, "JUL": 21.4, "AUG": 19.5, "SEP": 15.3, "OCT": 14.0, "NOV": 6.7, "DEC": 5.3, "WIN": 8.01,
        "SPR": 11.57, "SUM": 20.28, "AUT": 12.02, "ANN": 12.71
    },
    {
        "id": "5a88d224e02d8f7697e93f96", "dataset": "tmax", "region": "England", "Year": 1926, "JAN": 6.8,
        "FEB": 9.0, "MAR": 9.5, "APR": 13.0, "MAY": 14.0, "JUN": 17.7, "JUL": 20.8, "AUG": 20.7, "SEP": 18.5, "OCT": 11.7,
        "NOV": 8.8, "DEC": 6.5, "WIN": 6.95, "SPR": 12.18, "SUM": 19.75, "AUT": 13.01, "ANN": 13.11
    }
]