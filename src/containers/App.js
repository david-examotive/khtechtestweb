import React, { Component } from 'react';
import './App.css';
import 'array.prototype.fill'
import AllRegionsTemp from '../components/AllRegionsTemp.js'
import SunInScotland from '../components/SunInScotland.js'
import RainInWales from '../components/RainInWales.js'
import SeasonalRain from '../components/SeasonalRain.js'



class App extends Component {

  render() {
    return (
      <div style={{justifyContent: 'center', margin: "0"}} className="App">
        <header className="App-header">
          <h1 className="App-title">Data Visualisation Example Using React</h1>
        </header>
        <p className="App-intro">
          This project uses 'recharts' for React, a charting library for React (http://recharts.org/) built on D3.js
        </p>
        <p className="App-intro">
          The Slider component (https://github.com/react-component/slider) adjusts the year input of the graphs</p>
        <SeasonalRain />
        <AllRegionsTemp />
        <SunInScotland />
        <RainInWales />
      </div>
    );
  }
}
export default App;
