import React, { Component } from 'react';
import { AreaChart, XAxis, YAxis, Tooltip, Area } from 'recharts';
import axios from 'axios';

// http://localhost:8000/query?dataset=sunshine&region=Scotland


export default class SunInScotland extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sunLevel: [],
        }
    }

    updateFromServer() {
        this.serverRequest =
            axios
                .get("http://localhost:8000/query?dataset=sunshine&region=Scotland&min_year=1942")
                .then((result) => {
                    this.setState({
                        sunLevel: result.data
                    });
                })
    }

    componentDidMount() {
        this.updateFromServer();

    }

    componentWillUnmount() {
        this.serverRequest.abort();
    }

    render() {
        return (
            <div className="rainInWales" >

                <p className="App-intro">
                    This chart shows the annual sun levels in Scotland, post WWII.
            </p>
                <AreaChart
                    width={800}
                    height={400}
                    data={this.state.sunLevel}
                >
                    <XAxis dataKey="Year" />
                    <YAxis domain={[800, 1600]} />

                    <Tooltip />
                    <Area type='monotone' dataKey='ANN' stroke='#cc7a00' fill="#ffe680" />
                </AreaChart>
            </div>

        )

    }


}
