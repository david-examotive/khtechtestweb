import React, { Component } from 'react';
import { PieChart, Tooltip, Pie, Legend, Cell } from 'recharts';
import axios from 'axios';
import 'rc-slider/assets/index.css';
import Slider from 'rc-slider';
import { seasonSplit } from '../utils/data-translations.js'

const colors = ['#66CDAA', '#98FB98', '#3CB371', '#556B2F'];
const style = { width: "50%", padding: "40px" };
export default class SeasonalRain extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rainLevel: [],
            year_selected: 2017
        }
    }

    updateFromServer() {
        this.serverRequest =
            axios
                .get("http://localhost:8000/query?dataset=rainfall&region=UK&min_year=" + this.state.year_selected +
                    "&max_year=" + this.state.year_selected)
                .then((result) => {
                    this.setState({
                        rainLevel: seasonSplit(result.data)
                    });
                })
    }

    componentDidMount() {
        this.updateFromServer();

    }

    componentWillUnmount() {
        this.serverRequest.abort();
    }

    onSliderChange = (value) => {
        this.setState({ year_selected: value });
        this.updateFromServer();
    }

    render() {
        return (
            <div className="rainInWales" >
                <p className="App-intro">
                    This chart shows a single year's rainfall in the UK, divided into seasons.  Use the slider to change the year value.
                </p>
                <p className="App-intro">
                    Year Selected: {this.state.year_selected}        
                </p>
                <Slider style={style} min={1910} max={2018} defaultValue={2017} onChange={this.onSliderChange.bind(this)} />

                <PieChart width={800} height={400}>
                    <Pie isAnimationActive={false} data={this.state.rainLevel} dataKey="value" nameKey="season" outerRadius={120} fill="#8884d8" label>
                        {
                            this.state.rainLevel.map((entry, index) => (
                                <Cell key={`cell-${index}`} fill={colors[index]} />
                            ))
                        }
                    </Pie>
                    <Tooltip />
                    <Legend />
                </PieChart>
            </div>

        )

    }


}
