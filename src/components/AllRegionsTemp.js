import React, { Component } from 'react';
import { XAxis, YAxis, Tooltip, CartesianGrid, LineChart, Line } from 'recharts';
import axios from 'axios';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import { yearSeasonTranslation } from '../utils/data-translations.js'

const MAX_YEAR = 2018;
const style = { width: "50%", padding: "40px" };

export default class AllRegionsTemp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            yearSeason: [],
            min_year: MAX_YEAR - 10,
            max_year: MAX_YEAR
        }
    }

    updateFromServer() {
        this.serverRequest =
            axios
                .get("http://localhost:8000/query?dataset=tmax&region=UK|England|Wales|Scotland&min_year=" + this.state.min_year + "&max_year=" + this.state.max_year)
                .then((result) => {
                    this.setState({
                        yearSeason: yearSeasonTranslation(result.data)
                    });
                })
    }

    componentDidMount() {
        this.updateFromServer();

    }

    componentWillUnmount() {
        this.serverRequest.abort();
    }

    onSliderChange = (value) => {
        this.setState({ min_year: value - 10, max_year: value });
        this.updateFromServer();
    }

    render() {
        return (
            <div className="rainInWales" >
                            <p className="App-intro">
                    This chart shows a ten-year range of maximum temperatures for England and Scotland.  
                    Use the slider to change the year value.  Seasons are indicated starting with Spring (represented as '1')
                    For example, Spring of 1912 would be 1912.1
                </p>
                <p className="App-intro">
                    Years Selected: {this.state.min_year} - {this.state.max_year}        
                </p>
                <Slider style={style} min={1910} max={2018} defaultValue={2018} onChange={this.onSliderChange.bind(this)} />

                <LineChart width={800} height={300} data={this.state.yearSeason}>
                    <XAxis dataKey="season" />
                    <YAxis yAxisId="England" stroke="#8884d8" hide />
                    <YAxis yAxisId="Scotland" stroke="#82ca9d" hide />
                    <YAxis yAxisId="UK" stroke="#92ca3d" hide />
                    <YAxis yAxisId="Wales" stroke="#92ca3d" hide />
                    <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
                    <Tooltip />
                    <Line type="monotone" yAxisId="England" dataKey="England" stroke="#ff0000" />
                    <Line type="monotone" yAxisId="Scotland" dataKey="Scotland" stroke="#0000ff" />
                </LineChart>
            </div>

        )

    }


}
