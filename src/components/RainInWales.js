import React, { Component } from 'react';
import { BarChart, XAxis, YAxis, Tooltip, Bar, CartesianGrid } from 'recharts';
import axios from 'axios';
import 'rc-slider/assets/index.css';
import Slider from 'rc-slider';
import { monthSplit } from '../utils/data-translations.js'

const style = { width: "50%", padding: "40px" };

export default class RainInWales extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rainLevel: [],
            year_selected: 2017
        }
    }

    updateFromServer() {
        this.serverRequest =
            axios
                .get("http://localhost:8000/query?dataset=rainfall&region=Wales&min_year=" + this.state.year_selected + 
                "&max_year=" + this.state.year_selected)
                .then((result) => {
                    this.setState({
                        rainLevel: monthSplit(result.data)
                    });
                })
    }

    componentDidMount() {
        this.updateFromServer();

    }

    componentWillUnmount() {
        this.serverRequest.abort();
    }

    onSliderChange = (value) => {
        this.setState({year_selected: value});
        this.updateFromServer();
      }

    render() {
        return (
            <div className="rainInWales" >
                <p className="App-intro">
                    This chart shows a single year's rainfall for Wales.  Use the slider to change the year value.
                </p>
                <p className="App-intro">
                    Year Selected: {this.state.year_selected}        
                </p>
            <Slider style={style} min={1910} max={2018} defaultValue={2017} onChange={this.onSliderChange.bind(this)} />

            <BarChart
                width={800}
                height={400}
                data={this.state.rainLevel}
            ><CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="month"/>
                <YAxis />

                <Tooltip />
                <Bar type='monotone' dataKey='value' stroke='#007acc' fill="#33adff" />
            </BarChart>
            </div>

        )

    }


}
